import java.util.HashMap;
import java.util.Map;

public class OddOccurencesInArray {
	public int solution(int[] A) {
        int val=0;
        Map<Integer,Integer> map = new HashMap<Integer,Integer>();
        for(int i=0;i<A.length;i++)
        {
            if(null!=map.get(A[i]))
            {
                int c= map.get(A[i]);
                map.put(A[i],c+1);
            }
            else
            map.put(A[i],1);
        }

        for(Map.Entry<Integer,Integer> e : map.entrySet())
        {
            if(e.getValue()%2!=0)
            {
            val= e.getKey();
            break;
            }
        }
        return val;
    }

}
