import java.util.*;
public class MaxProductOfThree {
	 public int solution(int[] A) {
         int prod=0;
         Arrays.sort(A);   
         int len = A.length;
         int max = A[len-1];
         if(max<0)
         {
             prod=A[len-1]*A[len-2]*A[len-3];
         } else{  
        int fprod= A[0]*A[1];
        int lprod = A[len-2]*A[len-3];
        if(fprod>= lprod)
        prod=A[len-1]*fprod;
        else
        prod=A[len-1]*lprod;
         }
        return prod;
    }
}
