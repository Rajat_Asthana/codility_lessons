
public class MaxCounters {
	
	 public int[] solution(int N, int[] A) {
	        
	        int[] array = new int[N];
	        int max=0;
	        for(int x :A)
	        {
	          if(x>N)
	          {
	              for(int i=0;i<array.length;i++)
	              array[i]=max;
	          }
	          else
	          {
	              array[x-1]=array[x-1]+1;
	              if(max<array[x-1])
	              {
	                  max= array[x-1];
	              }
	          }

	        }
	        return array;
	    }

}
