import java.util.HashSet;
import java.util.Set;

public class Distinct {
	
	 public int solution(int[] A) {
	        // write your code in Java SE 8
	        int num=0;
	        Set<Integer> s = new HashSet<Integer>(); 
	        for(int i:A)
	        {
	            if(s.contains(i))
	            continue;
	            else
	            s.add(i);
	        }
	        num=s.size();
	        return num;
	    }
}
