import java.util.HashSet;
import java.util.Set;

public class FrogRiverOne {
	  public int solution(int X, int[] A) {
	        int pos=-1;
	        Set<Integer> s = new HashSet<Integer>();
	        for(int i =0 ; i<A.length;i++)
	        {
	            s.add(A[i]);
	            if(s.size()==X)
	            {
	                pos=i;
	                break;
	            }
	        }

	        return pos;
	    
	    }
}
