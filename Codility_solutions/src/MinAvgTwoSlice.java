 import java.util.*;
 import java.util.stream.Collectors;
public class MinAvgTwoSlice {
	public int solution(int[] A) {
	       
	       int len =A.length;
	       TreeMap<Double,Integer> map = new TreeMap<Double,Integer>();

	       for(int i = 0 ; i<len-1;i++)
	       {
	           int sum=A[i];
	           for(int j=i+1;j<len;j++)
	           {    
	               sum=sum+A[j];
	              double avg= (double)sum/(j-i+1);
	              map.putIfAbsent(avg,i);
	           }
	       }
	      // Set<Map.Entry<Double,Integer>> set=map.entrySet();
	      // for(Map.Entry<Double,Integer> entry:set)
	      // {
	      //      System.out.println(entry.getKey() + "=" +entry.getValue());
	      // }
	      Collection<Integer> arr= map.values();
	      List<Integer> lst=arr.stream().limit(1).collect(Collectors.toList());
	      return lst.get(0);
	    }
}
