import java.util.*;

public class permCheck {
	public int solution(int[] A) {
        int per =1;
        int N = A.length;
        Map<Integer,Integer> map = new HashMap<Integer,Integer> ();
        
        for(int x : A)
        {
            if(map.get(x)!=null)
            {
                per=0;
                break;
            }
            else
            map.put(x,1);
        }
        if(per==0)
        {
            return 0;
        }else
        {
            for(int i=1;i<=N;i++)
            {
                if(map.get(i)==null)
                {
                    per =0;
                    break;
                }
            }

            return per;
        }


    }

}
