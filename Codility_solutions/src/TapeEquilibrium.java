
public class TapeEquilibrium {
	  public int solution(int[] A) {
	        int left = A[0];
	        int right=0;
	        int diff= Integer.MAX_VALUE;
	        for(int i= 1; i< A.length;i++)
	        right+=A[i];

	        for(int i=1; i<A.length;i++)
	        {
	            int delta = Math.abs(left-right);
	            if(delta<diff)
	            diff=delta;
	            left=left+A[i];
	            right-=A[i];
	        }
	        return diff;
	    }
}
