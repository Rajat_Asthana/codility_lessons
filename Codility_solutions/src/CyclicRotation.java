
public class CyclicRotation {
	
	public int[] solution(int[] A, int K) {
        int size = A.length;
       int[] b = new int[size];
       if(K>size && size>0)
       {
           K=K%size;
       }

       for(int i=0;i<size;i++)
       {
           if(i+K>=size)
               b[i+K-size]=A[i];
           else
           b[i+K]=A[i];
       }
       return b;
   }

}
