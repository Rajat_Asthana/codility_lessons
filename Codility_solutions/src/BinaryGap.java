
public class BinaryGap {
	
	public int solution(int N) {
        int max =0;
        String binNum =Integer.toBinaryString(N);
        int fIndex = binNum.indexOf("1");
        int lIndex = binNum.lastIndexOf("1");
        String[] modNum = binNum.substring(fIndex,lIndex+1).split("1");
        for(String a :modNum)
        {
            if(a.length()>max)
            max=a.length();
        }
        return max;
    }

}
