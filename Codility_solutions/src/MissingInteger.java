import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class MissingInteger {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = new int[100000];
		for (int i=0;i<100000;i++)
			a[i]=i+1;
		
		int x = solution(a);
		System.out.println(x);
	}
	
	public static int solution(int[] A) {
		
		  Arrays.sort(A); int N=A.length;
		  
		  int max = A[N-1]; 
		  if(max<1) 
		  { 
			  return 1; 
		  }
		  else { 
			 HashSet<Integer> set = (HashSet<Integer>)Arrays.stream(A).boxed().filter(i->(Integer)i>=1).collect(Collectors.toSet());
			 ArrayList list= new ArrayList(set);
			 Collections.sort(list);

			  int count=1;
	          int output=0;
	          Iterator iter = list.iterator();
	          while(iter.hasNext())
	          {
	        	  int x=((Integer)iter.next()).intValue();
	        	 
	              if(count!=x)
	                {
	                    output=count;
	                    break;
	                }
	                count++;
	          }
	          if(output==0)
	          {
	              output=max+1;
	          }
	          return output;  
		  }
	}
}
