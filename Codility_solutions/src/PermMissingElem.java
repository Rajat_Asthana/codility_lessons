import java.util.Arrays;

public class PermMissingElem {
	 public int solution(int[] A) {
		 Arrays.sort(A);
	        int N = A.length;
	        int max = N+1;
	        if(N==0)
	        return 1;
	        else if(A[N-1]!=max)
	        return max;
	        else{
	            int value=1;
	            for(int i =0;i<N-1;i++)
	            {
	                if(A[i+1] - A[i]!=1){
	                value= A[i]+1;
	                break;
	                }
	            }
	            return value;
	        }
	    }
}
