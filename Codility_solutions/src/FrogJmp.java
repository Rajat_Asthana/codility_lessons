
public class FrogJmp {
	
	public int solution(int X, int Y, int D) {
	       int distance = Y-X;
	        int steps = distance/D;
	        int rem = distance%D;
	        if(rem!=0)
	        steps++;

	        return steps;
	    }

}
